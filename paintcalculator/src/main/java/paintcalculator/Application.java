package paintcalculator;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.runtime.Micronaut;
import io.reactivex.Single;
import paintcalculator.domain.AmountTotal;
import paintcalculator.domain.InputAmount;
import paintcalculator.service.CalculatorService;
import paintcalculator.service.TotalsOrchestrator;

import javax.inject.Inject;

@Controller(value = "/paint")
public class Application {

    @Inject
    private TotalsOrchestrator totals;

    @Get(value = "/calculateAmount",
            consumes = MediaType.APPLICATION_JSON,
            produces = MediaType.APPLICATION_JSON)
    public HttpResponse<AmountTotal> calculateAmount(@Body Single<InputAmount> inputAmount) {
        return HttpResponse.ok(totals.getAmountTotal(inputAmount));
    }

    public static void main(String[] args) {
        Micronaut.run(Application.class, args);
    }
}
