package paintcalculator.domain;

public final class InputAmount {

    public double height;
    public double width;
    public double length;

    public InputAmount withHeight(double height) {
        this.height = height;
        return this;
    }

    public InputAmount withWidth(double width) {
        this.width = width;
        return this;
    }

    public InputAmount withLength(double length) {
        this.length = length;
        return this;
    }
}
