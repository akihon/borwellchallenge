package paintcalculator.domain;

public final class AmountTotal {
    public double floorArea;
    public double paintTotal;
    public double roomVolume;


    public AmountTotal withFloorArea(double floorArea) {
        this.floorArea = floorArea;
        return this;
    }

    public AmountTotal withPaintTotal(double paintTotal) {
        this.paintTotal = paintTotal;
        return this;
    }

    public AmountTotal withRoomVolume(double roomVolume) {
        this.roomVolume = roomVolume;
        return this;
    }
}
