package paintcalculator.service;

import io.reactivex.Single;
import paintcalculator.domain.AmountTotal;
import paintcalculator.domain.InputAmount;

public interface TotalsOrchestrator {

    AmountTotal getAmountTotal(Single<InputAmount> amount);
}
