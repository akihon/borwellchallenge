package paintcalculator.service.logic;

import io.reactivex.Single;
import paintcalculator.domain.AmountTotal;
import paintcalculator.domain.InputAmount;
import paintcalculator.service.CalculatorService;
import paintcalculator.service.TotalsOrchestrator;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TotalsOrchestratorImpl implements TotalsOrchestrator {

    @Inject
    private CalculatorService service;

    @Override
    public AmountTotal getAmountTotal(Single<InputAmount> amount) {
        var singleAmount= amount.blockingGet();
        return new AmountTotal().withPaintTotal(service.getPaintAmount(singleAmount))
                .withFloorArea(service.getFloorArea(singleAmount)).withRoomVolume(service.getRoomVolume(singleAmount));
    }
}
