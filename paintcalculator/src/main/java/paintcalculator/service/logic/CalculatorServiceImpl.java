package paintcalculator.service.logic;

import paintcalculator.domain.InputAmount;
import paintcalculator.service.CalculatorService;

import javax.inject.Singleton;

@Singleton
public class CalculatorServiceImpl implements CalculatorService {

    @Override
    public double getFloorArea(InputAmount amount) {
        return amount.length * amount.width;
    }

    @Override
    public double getPaintAmount(InputAmount amount) {
        var areaSmallestWalls = (amount.height * amount.width) * 2;
        var areaLargestWalls = (amount.height * amount.length) * 2;
        return areaLargestWalls + areaSmallestWalls;
    }

    @Override
    public double getRoomVolume(InputAmount amount) {
        return amount.length * amount.width * amount.height;
    }
}
