package paintcalculator.service;

import com.oracle.svm.core.heap.ReferenceMapEncoder;
import paintcalculator.domain.InputAmount;

public interface CalculatorService {
    double getFloorArea(InputAmount amount);
    double getPaintAmount(InputAmount amount);
    double getRoomVolume(InputAmount amount);
}
