package paintcalculator;

import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import io.reactivex.Single;
import org.junit.jupiter.api.Test;
import paintcalculator.domain.InputAmount;
import paintcalculator.service.TotalsOrchestrator;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;

@MicronautTest
public class TotalsOrchestratorTest {

    @Inject
    private TotalsOrchestrator orchestrator;

    @Test
    public void testGetPopulatedAmountTotals() {
        var amount = orchestrator.getAmountTotal(Single.just(new InputAmount().withHeight(2).withWidth(3).withLength(5)));
        assertThat(amount).isNotNull();
        assertThat(amount.paintTotal).isEqualTo(32);
        assertThat(amount.floorArea).isEqualTo(15);
        assertThat(amount.roomVolume).isEqualTo(30);
    }
}
