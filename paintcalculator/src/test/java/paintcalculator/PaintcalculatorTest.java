package paintcalculator;

import io.micronaut.runtime.EmbeddedApplication;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import paintcalculator.domain.InputAmount;

import javax.inject.Inject;
import java.net.http.HttpClient;

@MicronautTest
public class PaintcalculatorTest {

    @Inject
    EmbeddedApplication<?> application;

    @Test
    void testItWorks() {
        Assertions.assertTrue(application.isRunning());
    }

}
