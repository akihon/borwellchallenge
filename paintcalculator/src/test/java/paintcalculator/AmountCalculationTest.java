package paintcalculator;

import com.oracle.svm.core.heap.ReferenceMapEncoder;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import paintcalculator.domain.InputAmount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import paintcalculator.service.CalculatorService;

import javax.inject.Inject;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@MicronautTest
public class AmountCalculationTest {

    @Inject
    private CalculatorService service;

    @ParameterizedTest(name = "Get correct amounts test")
    @MethodSource("inputsProvider")
    public void testGetCorrectAmounts(InputAmount inputAmount) {
        assertThat(service.getFloorArea(inputAmount)).isEqualTo(15);
        assertThat(service.getPaintAmount(inputAmount)).isEqualTo(32);
        assertThat(service.getRoomVolume(inputAmount)).isEqualTo(30);
    }

    static Stream<InputAmount> inputsProvider() {
        return Stream.of(new InputAmount().withHeight(2).withWidth(3).withLength(5));
    }

}
