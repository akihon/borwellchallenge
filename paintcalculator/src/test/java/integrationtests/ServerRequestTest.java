package integrationtests;

import com.google.gson.Gson;
import io.micronaut.context.ApplicationContext;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.client.HttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.Arguments;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import paintcalculator.domain.InputAmount;

import static org.assertj.core.api.Assertions.assertThat;

@MicronautTest
public class ServerRequestTest {

    private static EmbeddedServer server;
    private static HttpClient client;

    private InputAmount amount = new InputAmount().withHeight(2).withWidth(3).withLength(5);

    @BeforeClass
    public static void setupServer() {
        server = ApplicationContext.run(EmbeddedServer.class);
        client = server.getApplicationContext().createBean(HttpClient.class, server.getURL());
    }

    @AfterClass
    public static void stopServer() {
        if(server != null) {
            server.stop();
        }
    }

    @Test
    public void testGetCalculatedAmountWithValidBody() throws Exception {
        //out of time to write the integration test

        //assmuptions made test that the body is correctly processed and returned in the correct
        //object and format
    }
}
