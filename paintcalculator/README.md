## Assumptions before starting 

        //assume the walls only are being painted
        //assume all 4 walls are to be painted
        //assume all inputs do not include space for doors or windows

        //assume that time to paint the room is not in scope

        //assume that 1m^2 will take 1ltr of paint

## Feature http-client documentation

- [Micronaut Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)

## To build 

    Maven compile

## To test 
    
    mvn test 

## To build a native executable binary file using docker 

    mvn package -Dpackaging=docker-native 

## To Run 

    docker run -p 8080:8080 paintcalculator